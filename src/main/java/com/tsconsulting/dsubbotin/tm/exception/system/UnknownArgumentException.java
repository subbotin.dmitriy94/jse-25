package com.tsconsulting.dsubbotin.tm.exception.system;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Argument not found! Use command '-h'.");
    }

}
