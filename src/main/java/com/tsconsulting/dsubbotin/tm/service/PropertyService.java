package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "config.properties";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "1.0.0";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String DEVELOPER_NAME_DEFAULT = "Dima Subbotin";

    @NotNull
    private static final String DEVELOPER_NAME_KEY = "developer.name";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "subbotin.dmitriy94@gmail.com";

    @NotNull
    private static final String DEVELOPER_EMAIL_KEY = "developer.email";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getPropertyValue(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return getPropertyValue(DEVELOPER_NAME_KEY, DEVELOPER_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return getPropertyValue(DEVELOPER_EMAIL_KEY, DEVELOPER_EMAIL_DEFAULT);
    }

    @Override
    public int getPasswordIteration() {
        return Integer.parseInt(getPropertyValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getPropertyValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private String getPropertyValue(@NotNull final String property, @NotNull final String defaultValue) {
        if (System.getProperties().contains(property)) return System.getProperty(property);
        if (System.getenv().containsKey(property)) return System.getenv(property);
        return properties.getProperty(property, defaultValue);
    }

}
