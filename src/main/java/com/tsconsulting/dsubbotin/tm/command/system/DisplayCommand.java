package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class DisplayCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String name() {
        return "commands";
    }

    @Override
    @NotNull
    public String arg() {
        return "-cmd";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list commands.";
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            TerminalUtil.printMessage(command.name());
        }
    }

}
