package com.tsconsulting.dsubbotin.tm.api.entity;

public interface IWBS extends IHasCreated, IHasStartDate, IHasName, IHasStatus {
}
