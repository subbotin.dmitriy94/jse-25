package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IOwnerRepository;
import com.tsconsulting.dsubbotin.tm.model.AbstractOwnerEntity;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {
}