package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IRepository;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}