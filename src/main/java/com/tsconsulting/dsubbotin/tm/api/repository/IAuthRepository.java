package com.tsconsulting.dsubbotin.tm.api.repository;

import org.jetbrains.annotations.Nullable;

public interface IAuthRepository {

    @Nullable
    String getCurrentUserId();

    void setCurrentUserId(@Nullable String currentUserId);

}
